package co.simplon.exospring.Model;

import java.sql.Timestamp;

public class Subscriber {
    private int subscriberId;
    private String firstName;
    private String lastName;
    private String email;
    private Timestamp createdAt;

    
    public Subscriber(String firstName, String lastName, String email, Timestamp createdAt) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.createdAt = createdAt;
    }


    public Subscriber(int subscriberId, String firstName, String lastName, String email, Timestamp createdAt) {
        this.subscriberId = subscriberId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.createdAt = createdAt;
    }


    public Subscriber() {
    }


    public int getSubscriberId() {
        return subscriberId;
    }


    public String getFirstName() {
        return firstName;
    }


    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }


    public String getLastName() {
        return lastName;
    }


    public void setLastName(String lastName) {
        this.lastName = lastName;
    }


    public String getEmail() {
        return email;
    }


    public void setEmail(String email) {
        this.email = email;
    }


    public Timestamp getCreatedAt() {
        return createdAt;
    }


    public void setCreatedAt(Timestamp createdAt) {
        this.createdAt = createdAt;
    }

    @Override
    public String toString() {
        return "Subscriber [createdAt=" + createdAt + ", email=" + email + ", firstName=" + firstName + ", lastName="
                + lastName + ", subscriberId=" + subscriberId + "]";
    }
}
