package co.simplon.exospring.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import co.simplon.exospring.Model.Subscriber;

@Repository
public class SubscriberDAOImpl implements SubscriberDAO {

    @Autowired
    private DataSource dataSource;

    private static final String GET_SUBSCRIBERS = "SELECT * FROM subscribers";

    @Override
    public List<Subscriber> findAll() {
        
        Connection cnx;
        List<Subscriber> list = new ArrayList<>();

        try {

            cnx = dataSource.getConnection();
            PreparedStatement ps = cnx.prepareStatement(GET_SUBSCRIBERS);
            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                list.add(new Subscriber(
                    rs.getInt("SubscriberId"),
                    rs.getString("firstName"),
                    rs.getString("lastName"),
                    rs.getString("email"),
                    rs.getTimestamp("createdAt")
                ));
            } 
            cnx.close();
        } catch (Exception e) {
            e.getMessage();
        }

        return list;
    }

    
}
