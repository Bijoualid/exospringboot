package co.simplon.exospring.dao;

import java.util.List;

import co.simplon.exospring.Model.Subscriber;

public interface SubscriberDAO {
    
    public List<Subscriber> findAll();
}
