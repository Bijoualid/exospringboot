# Développer une application Spring Boot / Thymeleaf

## Specificites fonctionnelles

L'application doit permettre de :
    - Gérer des abonnés : 
        - consulter la liste des abonnés
        - consulter le détail d'un abonné
        - modifier le détail d'un abonné
        - ajouter un abonné
        - supprimer un abonné


## Specificites techniques

L'application doit :
    -prévoir des messages d'erreur pour guider l'utilisateur
    -être simple d'utilisation (navigation, choix des couleurs)
    -être web responsive et compatible avec les navigateurs web Chrome, Firefox et Safari
    -être basé sur le model MVC
    -être maintenable et reutilisable
    -être securisee (confidentialite des donnees personnelles des clients)


L'application sera composee de 3 couches : 
    -Couche Metier : Models
    -Couche Technique : DAO (Spring Data JDBC / SPRING Data JPA)
    -Couche Web : MVC (Thymeleaf, Bootstrap, Font Awesome)


OS : Linux

ENV : JDK 17

LANG : Java

IDE : Visual Studio Code

SGBDR : MySQL

FRAMEWORK : Sring Boot

DEPENDENCIES : 
    -Spring web
    -Spring Boot DevTools
    -MySQL Driver
    -Spring Data JDBC
    -Spring Data JPA
    -TestContainers
    -Thymeleaf
    -Spring Security
    -Thymeleaf Extra Spring Security 5
    -Validation

UTILS : 
    -Git - GitHub - GitLab
    -LucidChart
    -BalsamiqMockups
    -Trello
    -Bootstrap
    -Font Awesome
    -REST Client
